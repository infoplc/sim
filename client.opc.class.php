<?php

class opcClient {

    private $opcServer;

    function __construct() {}

    public function connect_with_opc_server($opcServer) {
        $this->opcServer = $opcServer;
    }

    public function read_status_from($db) {
        $this->opcServer->tags = $db->read_tags();
    }

    public function write_status_into($db) {
        $db->write_tags($this->opcServer->tags);
    }
}
