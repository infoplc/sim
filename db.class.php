<?php

class db {

    private $conn;

    public function __construct($server, $username, $password, $database) {
        // Creamos una conexión con la base de datos
        $this->conn = mysqli_connect($server, $username, $password, $database);

        // Comprobamos si se estableció la conexión o hubo un error
        if (!$this->conn)
            die("Conexión fallida: " . mysqli_connect_error());
    }
    
    public function read_tags() {
        $tags = array();
        $sql = "SELECT * FROM `tags`";
        foreach ( $this->conn->query($sql) as $row ) {
            $tags[] = array("id" => $row['id'], "name" => $row['name'], "value" => "");
        }
        return $tags;
    }

    public function write_tags($tags) {
        $sql_stack = array();
        foreach ($tags as $tag) {
            $sql_stack[] = "INSERT INTO `values` (`value`, `created_at`, `tag_id`) VALUES ('".$this->conn->escape_string($tag['value'])."', CURRENT_TIMESTAMP, '".$tag['id']."')";
        }
        $sql = implode(";", $sql_stack);

        if ($this->conn->multi_query($sql) !== TRUE) {
            die("Error: " . $this->conn->error);
        }
    }
}
