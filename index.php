<?php

$cfg = array("db" => 
                    array(
                        "server"    => "localhost",
                        "username"  => "root",
                        "password"  => "",
                        "database"  => "pfm_api"));
                          
/* * * * * */

require_once("plc.class.php");
require_once("server.opc.class.php");
require_once("client.opc.class.php");
require_once("db.class.php");

$db = new db($cfg['db']['server'], $cfg['db']['username'], $cfg['db']['password'], $cfg['db']['database']);

$plc = new plc();
$opc_server = new opcServer();
$opc_client = new opcClient();

$plc->connect_with_opc_server($opc_server);
$opc_client->connect_with_opc_server($opc_server);
$opc_client->read_status_from($db);
$plc->update_status();
$opc_client->write_status_into($db);
