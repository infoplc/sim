<?php

class plc {

    private $opcServer;
    private $tags;

    function __construct() { }

    public function connect_with_opc_server($opcServer) {
        $this->opcServer = $opcServer;
    }

    public function update_status() {
        $this->tags = $this->opcServer->tags;
        $this->generate_new_values();
    }

    private function generate_new_values() {
        foreach ($this->opcServer->tags as $key => $tag) {
            if (substr($tag['name'], 0, 2) == "d_")
                $this->opcServer->tags[$key]['value'] = rand(0, 1);
            elseif (substr($tag['name'], 0, 4) == "hex_")
                $this->opcServer->tags[$key]['value'] = "0x" . str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
            else
                $this->opcServer->tags[$key]['value'] = rand(0, 1000);
        }
    }
}
